'use strict'

const debug = require('debug')('platziverse:mqtt')
const mosca = require('mosca')
const redis = require('redis')
const chalk = require('chalk')
const db = require('platziverse-db')
const dbConfig = db.dbConfig()
const { parsePayload } = require('./utils')

const backend = {
  type: 'redis',
  redis,
  return_buffers: true
}

const settings = {
  port: 1883,
  backend
}

const server = new mosca.Server(settings)
const clients = new Map()

let Agent, Metric

server.on('clientConnected', client => {
  debug(`Client Connected ${client.id}`)
  clients.set(client.id, null)
})

server.on('clientDisonnected', async client => {
  const agent = clients.get(client.id)

  if (agent) {
    // mark Agent as disconnected
    agent.connected = false

    try {
      await Agent.createOrUpdate(agent)
    } catch (e) {
      return handleError(e)
    }
    server.publish({
      topic: 'agent/disconnected',
      payload: JSON.stringify({
        agent: {
          uuid: agent.uuid
        }
      })
    })
    // Borrar el agente de la lista de clientes
    clients.delete(client.id)
    debug(`Client ${client.id} associed to agent ${agent.uuid} was desconnected`)
  }
})

server.on('published', async (packet, client) => {
  switch (packet.topic) {
    case 'agent/connected':
    case 'agent/disconnected':
      debug(`${packet.payload}`)
      break
    case 'agent/message':
      debug(`${packet.payload}`)
      const payload = parsePayload(packet.payload)

      if (payload) {
        payload.agent.connected = true

        let agent
        try {
          agent = await Agent.createOrUpdate(payload.agent)
        } catch (e) {
          return handleError(e)
        }

        debug(`Agent ${agent.uuid} saved`)

        // Notificar que el agente se conecto
        if (!clients.get(client.id)) {
          clients.set(client.id, agent)
          server.publish({
            topic: 'agent/connected',
            payload: JSON.stringify({
              agent: {
                uuid: agent.uuid,
                username: agent.username,
                name: agent.name,
                hostname: agent.hostname,
                pid: agent.pid,
                connected: agent.connected
              }
            })
          })
        }

        const queries = []

        payload.metrics.forEach(metric => queries.push(Metric.create(agent.uuid, metric)))
        Promise.all(queries).then(value => {
          debug('Records saved:', value)
        }).catch(e => {
          return handleError(e)
        })
      }
      break
  }
})

server.on('ready', async () => {
  const services = await db(dbConfig).catch(handleFatalError)
  Agent = services.Agent
  Metric = services.Metric
  console.log(`${chalk.white('[platziverse-mqtt]')} server is running`)
})

server.on('error', handleFatalError)

function handleFatalError (err) {
  console.log(`${chalk.red('[Fatal Error]')} ${err.message}`)
  console.log(err.stack)
  process.exit(1)
}

function handleError (err) {
  console.log(`${chalk.yellow('[Error]')} ${err.message}`)
  console.log(err.stack)
}

process.on('uncaughtException', handleFatalError)
process.on('unhandledRejection', handleFatalError)
