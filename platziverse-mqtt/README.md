# platziverse mqtt

## `agent/connected`
```js
{
  agent: {
    uuid, // auto generar
    username, // definir por configuración
    name, // definir por configuración
    hostname, // obterner del sistema operativo
    pid  // obterner del proceso
  }
}
```
## `agent/disconnected`
```js
{
  agent: {
    uuid
  }
}
```

## `agent/message`
```js
{
  agent,
  metrics: [
    {
      type,
      value
    }
  ],
  timestamp // se genera cuando creamos el mensaje
}
```
