'use strict'

const debug = require('debug')('platziverse:db:dbConfig')
const setupDatabase = require('./lib/db')
const setupAgentModel = require('./models/agent')
const setupMetricModel = require('./models/metric')
const defaults = require('defaults')
const setupAgent = require('./lib/agent')
const setupMetric = require('./lib/metric')

module.exports = async function (config) {
  config = defaults(config, {
    dialect: 'sqlite',
    pool: {
      max: 10,
      min: 0,
      idle: 10000
    },
    query: {
      raw: true
    }
  })

  const sequelize = setupDatabase(config)
  const AgentModel = setupAgentModel(config)
  const MetricModel = setupMetricModel(config)

  AgentModel.hasMany(MetricModel)
  MetricModel.belongsTo(AgentModel)

  await sequelize.authenticate()

  if (config.setup) {
    await sequelize.sync({force: true})
  }

  const Agent = setupAgent(AgentModel)

  const Metric = setupMetric(MetricModel, AgentModel)

  return {
    Agent,
    Metric
  }
}

module.exports.dbConfig = options => {
  const config = {
    database: process.env.DB_NAME || 'platziverse',
    username: process.env.DB_USER || 'root',
    password: process.env.DB_PASS || '',
    host: process.env.DB_HOST || 'localhost',
    loggin: s => debug(s),
    dialect: 'mysql',
    setup: false,
    operatorsAliases: false
  }

  function extend (obj, values) {
    const clone = Object.assign({}, obj)
    return Object.assign(clone, values)
  }

  return extend(config, options)
}
