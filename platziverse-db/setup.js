'use strict'

const db = require('./')
const inquirer = require('inquirer')
const chalk = require('chalk')
const config = db.dbConfig({setup: true})

const prompt = inquirer.createPromptModule()

async function setup () {
  const flags = process.argv[2]

  if (flags !== '--yes') {
    const awnser = await prompt([{
      type: 'confirm',
      name: 'setup',
      message: 'This will destroy your database, are you sure?'
    }])

    if (!awnser.setup) {
      return console.log('Nothing Happend!')
    }
  }

  await db(config).catch(handleFatalError)

  console.log('success')
  process.exit(0)
}

function handleFatalError (err) {
  console.error(`${chalk.red('[Fatal error:]')}${err.message}`)
  console.error(err.stack)
  process.exit(1)
}

setup()
