'use strict'

const agent = {
  id: 1,
  uuid: 'yyy-yyy-yyy',
  username: 'fixture',
  name: 'mcampbell',
  hostname: 'test-host',
  pid: 0,
  connected: true,
  createdAt: new Date(),
  updatedAt: new Date()
}

const agents = [
  agent,
  extend(agent, {id: 2, uuid: 'yyy-yyy-yya', connected: false, username: 'platzi'}),
  extend(agent, {id: 3, uuid: 'yyy-yyy-yyc', username: 'petronila'}),
  extend(agent, {id: 4, uuid: 'yyy-yyy-yyb', username: 'mrojas'}),
  extend(agent, {id: 5, uuid: 'yyy-yyy-yyc', connected: false, username: 'pantro'})
]

function extend (obj, values) {
  const clone = Object.assign({}, obj)
  return Object.assign(clone, values)
}

module.exports = {
  single: agent,
  all: agents,
  connected: agents.filter(a => a.connected),
  mcampbell: agents.filter(a => a.username === 'mcampbell'),
  byUuid: id => agents.filter(a => a.uuid === id).shift(),
  byId: id => agents.filter(a => a.id === id).shift()
}
