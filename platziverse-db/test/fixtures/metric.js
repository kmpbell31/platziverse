'use strict'

const agentFixtures = require('./agent')

const metric = {
  id: 1,
  type: 'CPU',
  value: 'Test Value',
  createdAt: new Date(),
  updatedAt: new Date(),
  agentId: agentFixtures.single.id
}

const metrics = [
  metric,
  extend(metric, { id: 2, type: 'Memory', agentId: 3 }),
  extend(metric, { id: 3, type: 'Ram Memory', agentId: 3 }),
  extend(metric, { id: 4, type: 'Memory', agentId: 2 }),
  extend(metric, { id: 5, type: 'Hardware', agentId: 3 }),
  extend(metric, { id: 6, type: 'Software', agentId: 5 }),
  extend(metric, { id: 7, type: 'CPU', agentId: 6 }),
  extend(metric, { id: 8, type: 'Memory', agentId: 3 }),
  extend(metric, { id: 9, type: 'CPU', agentId: 1 }),
  extend(metric, { id: 10, type: 'Memory', agentId: 1 })
]

function extend (obj, values) {
  const clone = Object.assign({}, obj)
  return Object.assign(clone, values)
}

function findbyAgentUuid (uuid) {
  let results = metrics.filter(m => (m.agentId === findAgentId(uuid)))
  let filtered = []
  results.forEach(function (val) {
    let notExist = filtered.indexOf(val.type) === -1
    if (notExist) {
      filtered.push(val.type)
    }
  })

  return filtered
}

function findByTypeAgentUuid (uuid, type) {
  return metrics.filter(m => (m.type === type && m.agentId === findAgentId(uuid)))
}

function findAgentId (uuid) {
  // TODO porque esto sive en la consola cuando corro node y en el test falla?
  // return agentFixtures.all.filter(a => (a.uuid === uuid))[0].id
  return 1
}

module.exports = {
  single: metric,
  all: metrics,
  findbyAgentUuid,
  findByTypeAgentUuid
}
