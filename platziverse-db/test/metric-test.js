'use strict'

const test = require('ava')
const sinon = require('sinon')
const proxyquire = require('proxyquire')

const metricFixtures = require('./fixtures/metric')
const agentFixtures = require('./fixtures/agent')

let config = {
  logging () {}
}

let MetricStub = null

let AgentStub = {
  hasMany: sinon.spy()
}

let type = 'CPU'
let db = null
let sandbox = null
let uuid = 'yyy-yyy-yyy'

let uuidArgs = {
  where: { uuid }
}

let agentUuidArgs = {
  attributes: [ 'type' ],
  group: [ 'type' ],
  include: [{
    attributes: [],
    model: AgentStub,
    where: {
      uuid
    }
  }],
  raw: true
}

let typeAgentUuid = {
  attributes: ['id', 'type', 'value', 'createdAt'],
  where: {
    type
  },
  limit: 20,
  order: [ [ 'createdAt', 'DESC' ] ],
  include: [{
    attributes: [],
    model: AgentStub,
    where: {
      uuid
    }
  }],
  raw: true
}

let newMetric = {
  type: 'NEW',
  value: 'Test Value',
  createdAt: new Date(),
  updatedAt: new Date()
}

test.beforeEach(async () => {
  sandbox = sinon.sandbox.create()

  MetricStub = {
    belongsTo: sandbox.spy()
  }

  // MetricModel Create
  MetricStub.create = sandbox.stub()
  MetricStub.create.withArgs(newMetric).returns(Promise.resolve({
    toJSON () { return newMetric }
  }))

  // AgentModel findOne
  AgentStub.findOne = sandbox.stub()
  AgentStub.findOne.withArgs(uuidArgs).returns(Promise.resolve(agentFixtures.single))

  // MetricModel findAll
  MetricStub.findAll = sandbox.stub()
  MetricStub.findAll.withArgs(agentUuidArgs).returns(Promise.resolve(metricFixtures.findbyAgentUuid(uuid)))
  MetricStub.findAll.withArgs(typeAgentUuid).returns(Promise.resolve(metricFixtures.findByTypeAgentUuid(uuid, type)))

  const setupDatabase = proxyquire('../', {
    './models/agent': () => AgentStub,
    './models/metric': () => MetricStub
  })

  db = await setupDatabase(config)
})

test.afterEach(async () => {
  sandbox && sinon.sandbox.restore()
})

test('Metric', t => {
  t.truthy(db.Metric, 'Metric service should exist')
})

test.serial('Setup', t => {
  t.true(AgentStub.hasMany.called, 'AgentModel.hasMany was executed')
  t.true(AgentStub.hasMany.calledWith(MetricStub), 'Argument should be the MetricModel')
  t.true(MetricStub.belongsTo.called, 'MetricModel.belongsTo was executed')
  t.true(MetricStub.belongsTo.calledWith(AgentStub), 'Argument should be the AgentModel')
})

test.serial('Metric#create', async t => {
  let metric = await db.Metric.create(uuid, newMetric)

  t.true(AgentStub.findOne.called, 'findOne should be called on model')
  t.true(AgentStub.findOne.calledOnce, 'findOne should be called once')
  t.true(AgentStub.findOne.calledWith(uuidArgs), 'findOne should be called with args')
  t.true(MetricStub.create.called, 'create should be called')
  t.true(MetricStub.create.calledOnce, 'create should be called once')
  t.true(MetricStub.create.calledWith(newMetric), 'create should be called with the specified arguments')

  t.deepEqual(metric, newMetric, 'Should be the same')
})

test.serial('Metric#findByAgentUuid', async t => {
  let metric = await db.Metric.findByAgentUuid(uuid)

  t.true(MetricStub.findAll.called, 'findAll should be called on model on findByAgentUuid')
  t.true(MetricStub.findAll.calledOnce, 'findAll should be called once on findByAgentUuid')
  t.true(MetricStub.findAll.calledWith(agentUuidArgs), 'findAll should be called with args')

  t.deepEqual(metric, metricFixtures.findbyAgentUuid(uuid), 'Should be the same')
})

test.serial('Metric#findByTypeAgentUuid', async t => {
  let metric = await db.Metric.findByTypeAgentUuid(uuid, type)

  t.true(MetricStub.findAll.called, 'findAll should be called on model on findByTypeAgentUuid')
  t.true(MetricStub.findAll.calledOnce, 'findAll should be called once on findByTypeAgentUuid')
  t.true(MetricStub.findAll.calledWith(typeAgentUuid), 'findAll should be called with args')

  t.deepEqual(metric, metricFixtures.findByTypeAgentUuid(uuid, type), 'Should be the same')
})
